'use strict';

let expect = require('chai').expect,
  thinky = require('../../../config/thinky');

describe('configure thinky', function() {
  it('should load', function() {
    expect(thinky).to.be.a('object');
  });
});
