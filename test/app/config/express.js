'use strict';

let expect = require('chai').expect,
  express = require('express'),
  configure = require('../../../config/express');

describe('configure express', function() {
  it('should load', function() {
    expect(configure).to.be.a('function');
  });

  it('should return the app', function() {
    let app = express();

    expect(configure(app, {})).to.eql(app);
  });
});