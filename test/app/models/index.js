'use strict';

var expect = require('chai').expect;

var model = require('../../../app/models/index');

describe('model', function() {
  it('should load', function() {
    expect(model).to.be.a('object');
    expect(model.AdminRole).to.exist;
    expect(model.ArchivedOrder).to.exist;
    expect(model.City).to.exist;
    expect(model.Country).to.exist;
    expect(model.Currency).to.exist;
    expect(model.Diet).to.exist;
    expect(model.ErrorLog).to.exist;
    expect(model.Item).to.exist;
    expect(model.ItemLog).to.exist;
    expect(model.Menu).to.exist;
    expect(model.Order).to.exist;
    expect(model.PointOfSale).to.exist;
    expect(model.Shop).to.exist;
    expect(model.ShopAdmin).to.exist;
    expect(model.SubMenu).to.exist;
    expect(model.Tag).to.exist;
    expect(model.User).to.exist;
    expect(model.Vat).to.exist;
  });
});
