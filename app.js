const express = require('express');
const config = require('./config/config');

const app = express();

module.exports = require('./config/express')(app, config);

app.listen(config.port, function () {
  console.log('Express server running ' + (process.env.NODE_ENV ? process.env.NODE_ENV : 'development') + ' listening on port ' + config.port);
});

require('./config/wss')().listen(config.wssPort, function () {
  console.log('Wss server listening on port ' + config.wssPort);
});
