var thinky = require('../../config/thinky'),
  r = thinky.r,
  type = thinky.type;

var Country = thinky.createModel('Country', {
  name: type.string().required().min(1)
});

module.exports = Country;

var City = require('./city');

Country.hasMany(City, 'cities', 'id', 'countryId');