var thinky = require('../../config/thinky'),
  r = thinky.r,
  type = thinky.type;

var ErrorLog = thinky.createModel('ErrorLog', {
  subject: type.string().required(),
  file: type.string().required().min(1),
  function: type.string().required().min(1),
  text: type.string().required(),
  handled: type.boolean().required().default(),
  type: type.number().optional()
});

module.exports = ErrorLog;