var thinky = require('../../config/thinky'),
  r = thinky.r,
  type = thinky.type;

var City = thinky.createModel('City', {
  name: type.string().required().min(1),
  countryId: type.string().required().min(1),
  lon: type.string().required().min(1),
  lat: type.string().required().min(1)
});

module.exports = City;

var Shop = require('./shop');
var Country = require('./country');

City.hasMany(Shop, 'shops', 'id', 'cityId');
City.belongsTo(Country, 'country', 'countryId', 'id');