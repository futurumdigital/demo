module.exports = (lang) => {
  if (lang)
    return require("./" + lang);
  else 
    return require("./sv_SE");
}