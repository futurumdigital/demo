/**
 * @module v1/cities 
 * @description REST API routes for handling cities. Responsible for setting and getting cities.
 * @exports express.Router
 */

const express = require('express');
const models = require('../../models');
const errorHandler = require('../../lib/error');
const jwt = require('../../middleware/jwt');

const City = models.City;
const router = express.Router();

module.exports = function (app) {
  app.use('/v1/cities', router);
};

/**
 * @method GET /v1/cities/:cityId/shops
 * @description Retrieves City with all shops
 * @return {object} == data: [{
 *  id: String,
 *  name: String,
 *  lon: String,
 *  lat: String,
 *  currentOffer: String,
 *  hasTakeAway: Boolean,
 *  openingHours: {
 *    mon: [{
 *      open: String,
 *      close: String
 *    }],
 *    tue: [{
 *      open: String,
 *      close: String
 *    }],
 *    wed: [{
 *      open: String,
 *      close: String
 *    }],
 *    thu: [{
 *      open: String,
 *      close: String
 *    }],
 *    fri: [{
 *      open: String,
 *      close: String
 *    }],
 *    sat: [{
 *      open: String,
 *      close: String
 *    }],
 *    sun: [{
 *      open: String,
 *      close: String
 *    }]
 *  },
 *  tags: [{
 *    name: String,
 *    order: Number,
 *    id: String
 *  }]
 * }]
 */
router.get('/:cityId/shops', function (req, res, next) {
  City.get(req.params.cityId).getJoin({ shops: { tags: true } }).run().then(function (city) {
    city.shops.forEach((shop, ind, arr) => {
      if (shop.displayInApp) {
        delete shop.displayInApp;
        shop.prepareAll();
      } else {
        arr.splice(ind, 1);
      }
    });
    return res.status(200).json({
      data: city.shops
    });
  }).catch(Errors.DocumentNotFound, function (error) {
    return res.status(404).json({
      message: 'not_found',
    });
  }).catch(function (error) {
    errorHandler.handle({ subject: 'Error retrieving shops from city', message: error.message }, errorHandler.urgencyLevel.MINOR);
    return res.status(500).json({});
  });
});

/**
 * @method GET /v1/cities/:cityId
 * @description Retrieves a City
 * @return {object} data: {
 *  name: String,
 *  countryId: String,
 *  lon: String,
 *  lat: String,
 *  id: String
 */
router.get('/:cityId', function (req, res, next) {
  City.get(req.params.cityId).run().then(function (city) {
    return res.status(200).json({
      data: city
    });
  }).catch(Errors.ValidationError, function (error) {
    return res.status(400).json({
      message: error.message,
    });
  }).catch(Errors.DocumentNotFound, function (error) {
    return res.status(404).json({
      message: 'not_found',
    });
  }).catch(function (error) {
    errorHandler.handle({ subject: 'Error retrieving city', message: error.message }, errorHandler.urgencyLevel.MINOR);
    return res.status(500).json({});
  });
});

router.use(jwt);

/**
 * @method POST /v1/cities
 * @description Creates a new city
 * @param {object} body body: {
 *  name: String,
 *  countryId: String,
 *  lon: String,
 *  lat: String
 * }
 * @return {object} data string representation of city id
 */
router.post('/', function (req, res, next) {
  if (req.decoded.role >= 10) {
    let city = new City({
      name: req.body.name,
      countryId: req.body.countryId,
      lon: req.body.lon,
      lat: req.body.lat
    });

    city.save(function (cityDoc) {
      return res.status(201).json({ data: cityDoc.id });
    }).catch(Errors.DocumentNotFound, function (error) {
      return res.status(404).json({
        message: 'not_found',
      });
    }).catch(function (error) {
      errorHandler.handle({ subject: 'Error saving city', message: error.message }, errorHandler.urgencyLevel.MINOR);
      return res.status(500).json({
        message: 'generic_error',
      });
    });
  } else {
    return res.status(401).json({});
  }
});

/**
 * @method DELETE /v1/cities/:cityId
 * @description Removes the specified city
 */
router.delete('/:cityId', function (req, res, next) {
  if (req.decoded.role >= 10) {City.get(req.params.cityId).run().then(function (city) {
      city.delete().then(function (result) {
        if (city.isSaved()) {
          return res.status(500).json({
            message: 'generic_error',
          });
        } else {
          return res.status(200).json({});
        }
      });
    }).catch(Errors.DocumentNotFound, function (error) {
      return res.status(404).json({
        message: 'not_found',
      });
    }).catch(function(error) {
      errorHandler.handle({subject: 'Error deleting city', message: error.message}, errorHandler.urgencyLevel.MINOR);
      return res.status(500).json({
        message: 'generic_error',
      });
    });
  } else {
    return res.status(401).json({});
  }
});

/**
 * @method PUT /v1/cities/:cityId
 * @description Updates specified city
 * @param {object} body body: {
 *  name: String,
 *  countryId: String,
 *  lon: String,
 *  lat: String
 * }
 * @return {object} status 200 ok, or 401/500 error 
 */
router.put('/:cityId', function (req, res, next) {
  if (req.decoded.role >= 10) {
    City.get(req.params.cityId).run().then(function (city) {
      if (req.body.name) {
        city.name = req.body.name;
      }
      if (req.body.countryId) {
        city.countryId = req.body.countryId;
      }
      if (req.body.lon) {
        city.lon = req.body.lon;
      }
      if (req.body.lat) {
        city.lat = req.body.lat;
      }

      city.saveAll().then(function (cityDoc) {
        return res.status(200).json({});
      }).catch(Errors.ValidationError, function (error) {
        return res.status(400).json({
          message: error.message,
        });
      }).catch((err) => {
        errorHandler.handle({ subject: 'Error updating city', message: error.message }, errorHandler.urgencyLevel.MINOR);
        return res.status(500).json({
          message: 'generic_error',
        });
      });
    }).catch(Errors.DocumentNotFound, function (error) {
      return res.status(404).json({
        message: 'not_found',
      });
    }).catch((err) => {
      errorHandler.handle({ subject: 'Error getting city', message: error.message }, errorHandler.urgencyLevel.MINOR);
      return res.status(500).json({
        message: 'generic_error',
      });
    });
  } else {
    return res.status(401).json({});
  }
});
