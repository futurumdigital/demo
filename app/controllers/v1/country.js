/**
 * @module v1/countries 
 * @description REST API routes for handling countries. Responsible for setting and getting countries.
 * @exports express.Router
 */
const express = require('express');
const models = require('../../models');
const errorHandler = require('../../lib/error');
const jwt = require('../../middleware/jwt');

const { Country } = models;
const router = express.Router();

module.exports = function (app) {
  app.use('/v1/countries', router);
};

/**
 * @method GET /v1/countries
 * @description Retrieves all countries
 * @return {object} data: [{
 *  name: String,
 *  id: String
 * }]
 */
router.get('/', function (req, res, next) {
  Country.run().then(function (countries) {
    return res.status(200).json({
      data: countries
    });
  }).catch(function (error) {
    errorHandler.handle({ subject: 'Error retrieving countries', message: error.message }, errorHandler.urgencyLevel.MINOR);
    return res.status(500).json({
      message: 'generic_error',
    });
  });
});

/**
 * @method GET /v1/countries/:countryId/cities
 * @description Retrieves all cities in a country
 * @return {object} data: [{
 *    name: String,
 *    id: String
 *  }]
 */
router.get('/:countryId/cities', function (req, res, next) {
  Country.get(req.params.countryId).getJoin({ cities: true }).run().then(function (country) {
    return res.status(200).json({
      data: country.cities
    });
  }).catch(Errors.DocumentNotFound, function (error) {
    return res.status(404).json({
      message: 'not_found',
    });
  }).catch(function (error) {
    errorHandler.handle({ subject: 'Error retrieving cities from country', message: error.message }, errorHandler.urgencyLevel.MINOR);
    return res.status(500).json({
      message: 'generic_error',
    });
  });
});

router.use(jwt);

/**
 * @method GET /v1/countries/:countryId
 * @description Retrieves a single country from provided countryId
 * @return {object} data: [{
 *  name: String,
 *  id: String
 * }]
 */
router.get('/:countryId', function (req, res, next) {
  Country.get(req.params.countryId).run().then(function (country) {
    return res.status(200).json({
      data: country
    });
  }).catch(Errors.DocumentNotFound, function (error) {
    return res.status(404).json({
      message: 'not_found',
    });
  }).catch(function (error) {
    errorHandler.handle({ subject: 'Error retrieving country', message: error.message }, errorHandler.urgencyLevel.MINOR);
    return res.status(500).json({
      message: 'generic_error',
    });
  });
});

/**
 * @method POST /v1/countries
 * @description Creates a new country
 * @param {object} body body: {
 *  name: String
 * }
 * @return {string} id the created country id 
 */
router.post('/', function (req, res, next) {
  if (req.decoded.role >= 10) {
    let country = new Country({
      name: req.body.name
    });

    country.save().then(function (countryDoc) {
      return res.status(201).json({ data: countryDoc.id });
    }).catch(Errors.ValidationError, function (error) {
      return res.status(400).json({
        message: error.message,
      });
    }).catch(function (error) {
      errorHandler.handle({ subject: 'Error saving country', message: error.message }, errorHandler.urgencyLevel.MINOR);
      return res.status(500).json({
        message: 'generic_error',
      });
    });
  } else {
    return res.status(401).json({});
  }
});

/**
 * @method DELETE /v1/countries/:countryId
 * @description Removes the specified country
 */
router.delete('/:countryId', function (req, res, next) {
  if (req.decoded.role >= 10) {
    Country.get(req.params.countryId).run().then(function (country) {
      country.delete().then(function (result) {
        if (country.isSaved()) {
          return res.status(500).json({
            message: 'could_not_delete',
          });
        } else {
          return res.status(200).json({});
        }
      });
    }).catch(Errors.DocumentNotFound, function (error) {
      return res.status(404).json({
        message: 'not_found',
      });
    }).catch(function (error) {
      errorHandler.handle({ subject: 'Error deleting country', message: error.message }, errorHandler.urgencyLevel.MINOR);
      return res.status(500).json({
        message: 'generic_error',
      });
    });
  } else {
    return res.status(401).json({});
  }
});

/**
 * @method PUT /v1/countries/:countryId
 * @description Updates the specified country
 * @param {object} body body: {
 *  name: String
 * }
 */
router.put('/:countryId', function (req, res, next) {
  if (req.decoded.role >= 10) {
    Country.get(req.params.countryId).run().then(function (country) {
      if (req.body.name) {
        country.name = req.body.name;
        country.saveAll().then(function (countryDoc) {
          return res.status(200).json({});
        }).catch(Errors.ValidationError, function (error) {
          return res.status(400).json({
            message: error.message,
          });
        }).catch(function (error) {
          errorHandler.handle({ subject: 'Error updating country', message: error.message }, errorHandler.urgencyLevel.MINOR);
          return res.status(500).json({
            message: 'generic_error',
          });
        });
      } else {
        return res.status(400).json({});
      }
    }).catch(Errors.DocumentNotFound, function (error) {
      return res.status(404).json({
        message: 'not_found',
      });
    }).catch(Errors.ValidationError, function (error) {
      return res.status(400).json({
        message: error.message,
      });
    }).catch(function (error) {
      errorHandler.handle({ subject: 'Error retriveing country', message: error.message }, errorHandler.urgencyLevel.MINOR);
      return res.status(500).json({
        message: 'generic_error',
      });
    });
  } else {
    return res.status(401).json({});
  }
});
