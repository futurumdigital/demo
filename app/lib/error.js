const mail = require('./mail');
const config = require('../../config/config');
const models = require('../models');

const ErrorLog = models.ErrorLog;
const urgencyLevel = {
  MINOR: 1,
  MEDIUM: 2,
  HIGH: 3
};

function isObject(o) {
  return Object.prototype.toString.call(o) === '[object Object]';
}

function handle(error, urgency) {
  try {
    if (isObject(error)) {
      if (!urgency) {
        urgency = urgencyLevel.MINOR;
      }
      if (!error.subject) {
        error.subject = "undefined subject";
      }
      if (!error.message) {
        error.message = "undefined message";
      }
      let text = error.message;
      if (isObject(text)) {
        text = JSON.stringify(text);
      } else if (text instanceof Error) {
        text = text.toString();
      }
      switch (urgency) {
        case urgencyLevel.HIGH:
          //mail.sendMail(config.supportEmail, error.subject, text);
        case urgencyLevel.MEDIUM:
        case urgencyLevel.MINOR:
          let errorLog = new ErrorLog({
            subject: error.subject,
            file: 'NYI',
            function: 'NYI',
            text,
            handled: false,
            type: urgency
          });
          errorLog.save().then().catch((e) => {
            console.log(e);
          });
        default:
          console.log(error);
          break;
      }
    } else {
      if (!urgency) {
        urgency = urgencyLevel.MINOR;
      }
      handle({
        subject: "Wrongly formated error",
        message: JSON.stringify(error)
      }, urgency);
    }
  } catch(e) {
    console.log("Crashing in error handler!");
    console.log(e);
    console.log(error);
  };
}

function handleSocket(error) {
  if (error) {
    handle({
      subject: "Socket error",
      message: JSON.stringify(error)
    }, urgencyLevel.MINOR);
  }
}

module.exports = {
    handle,
    urgencyLevel,
    handleSocket
}